//This file contains the handler for the /login/oauth/access_token route.
import 'package:copilot_proxy/check_auth/check_auth.dart';
import 'package:copilot_proxy/common.dart';
import 'package:copilot_proxy/context.dart';

Future<void> postLoginOauthAccessToken(Context context) async {
  removeDeviceCode(context['device_code']);
  final accessToken = uuid.v4();
  addAccessToken(accessToken, context['client_id']);
  context.ok();
  context.json({
    'access_token': accessToken,
    'scope': 'user:email',
    'token_type': 'bearer',
  });
}
